// from react-chart-js acquire the doughnut chart diagram
import { Doughnut } from 'react-chartjs-2';

// create the function to describe the anatomy of the component
export default function Chart() {
	return(
		<Doughnut 
			data={{
				labels: ['Criticals', 'Deaths', 'Recoveries']
			}}
			redraw={ false }
		/>
	)
}