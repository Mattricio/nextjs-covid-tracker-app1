// we will create a logic that will convert the string API result to numeric data types

// we want to use this in top.js

export default function toNum(str){
	// convert the strings to an array to gain access to array methods
	let arr = [...str]

	// visualize= ["", "", ""]
	// lets filter out the commas in the strings
	const elementNaWalangComma = arr.filter(element => element !== ",")
	// visualize= ["" "" ""]
	// reduce the filtered array back into a single string

	// this string can now be parsed into a number via the parseInt method
	return parseInt(elementNaWalangComma.reduce((x, y) => x + y))
}